package com.m2b2c.api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * User:Jiahengfei --> 17515250730@163.com
 * create by 2018/4/29 from idea.
 * describe:
 */
@RestController
@RequestMapping(value = "test")
public class TestController {

    @RequestMapping(value = "boot")
    public String testBoot(){
        return "Hello to SpringBoot! 你好!";
    }

}
